//! Functions on ref-generic data

use core::ops::DerefMut;

use crate::generic::{Fixed, RefGenericDataType, SingleRef};
use crate::reflect::{RefKindReflect, Reflect};
use crate::*;

/// A ref-generic version of [`FnOnce`]
pub trait RefGenericFnOnce<'a, 'b, In: RefGenericDataType<'a>, Out: RefGenericDataType<'b>> {
	#[allow(missing_docs)]
	fn call<K: RefKind>(self, input: In::Inst<K>) -> Out::Inst<K>;
}

/// A ref-generic version of [`Fn`] (when `FK = Shared`) and [`FnMut`] (when `FK = Unique`)
pub trait RefGenericFnK<
	'a,
	'b,
	FK: RefKind,
	In: RefGenericDataType<'a>,
	Out: RefGenericDataType<'b>,
>
{
	#[allow(missing_docs)]
	fn call<K: RefKind>(this: FK::Ref<'_, Self>, input: In::Inst<K>) -> Out::Inst<K>;
}

/// Generic adapter to facilitate the conversions `RefGenericFnK<Shared> => RefGenericFnK<Unique> => RefGenericFnOnce`
#[repr(transparent)]
#[derive(Debug, Copy, Clone, Eq, PartialEq, Hash)]
pub struct Stricten<F>(pub F);

impl<'a, 'b, In, Out, F> RefGenericFnOnce<'a, 'b, In, Out> for Stricten<F>
where
	In: RefGenericDataType<'a>,
	Out: RefGenericDataType<'b>,
	F: RefGenericFnK<'a, 'b, Unique, In, Out>,
{
	#[inline]
	fn call<K: RefKind>(mut self, input: In::Inst<K>) -> Out::Inst<K> {
		RefGenericFnK::<'a, 'b, Unique, In, Out>::call(&mut self.0, input)
	}
}

impl<'a, 'b, In, Out, F> RefGenericFnK<'a, 'b, Unique, In, Out> for Stricten<F>
where
	In: RefGenericDataType<'a>,
	Out: RefGenericDataType<'b>,
	F: RefGenericFnK<'a, 'b, Shared, In, Out>,
{
	#[inline]
	fn call<K: RefKind>(this: &mut Self, input: In::Inst<K>) -> Out::Inst<K> {
		RefGenericFnK::<'a, 'b, Shared, In, Out>::call(&this.0, input)
	}
}

/// Generic base struct to allow one to use a pair of functions (one for shared, one for unique)
/// as a `RefGenericFn*`
#[derive(Debug, Copy, Clone, Eq, PartialEq, Hash)]
pub struct ChooseFn<F, G> {
	/// The function to apply to [`Shared`] data
	pub shared: F,
	/// The function to apply to [`Unique`] data
	pub unique: G,
}

/// A macro for easy construction of [`ChooseFn`](struct@ChooseFn)
///
/// This constructs two *functions*, which usually works better when closures produce incomprehensible lifetime errors
#[macro_export]
#[allow(non_snake_case)]
macro_rules! ChooseFn {
    (
		shared: fn |$sin:ident: $sty:ty| -> $sr:ty $sb:block,
		unique: fn |$uin:ident: $uty:ty| -> $ur:ty $ub:block $(,)?
	) => {
		{
			fn shared($sin: $sty) -> $sr $sb
			fn unique($uin: $uty) -> $ur $ub

			$crate::func::ChooseFn { shared, unique }
		}
	};
}

impl<'a, 'b, In, Out, F, G> RefGenericFnOnce<'a, 'b, In, Out> for ChooseFn<F, G>
where
	In: RefGenericDataType<'a>,
	Out: RefGenericDataType<'b>,
	F: FnOnce(In::Inst<Shared>) -> Out::Inst<Shared>,
	G: FnOnce(In::Inst<Unique>) -> Out::Inst<Unique>,
{
	#[inline]
	fn call<K: RefKind>(self, input: In::Inst<K>) -> Out::Inst<K> {
		match K::reflect() {
			RefKindReflect::Shared(is) => is.ltrtl::<In, Out>(input, self.shared),
			RefKindReflect::Unique(is) => is.ltrtl::<In, Out>(input, self.unique),
		}
	}
}

impl<'a, 'b, In, Out, F, G> RefGenericFnK<'a, 'b, Shared, In, Out> for ChooseFn<F, G>
where
	In: RefGenericDataType<'a>,
	Out: RefGenericDataType<'b>,
	F: Fn(In::Inst<Shared>) -> Out::Inst<Shared>,
	G: Fn(In::Inst<Unique>) -> Out::Inst<Unique>,
{
	#[inline]
	fn call<K: RefKind>(this: &Self, input: In::Inst<K>) -> Out::Inst<K> {
		match K::reflect() {
			RefKindReflect::Shared(is) => is.ltrtl::<In, Out>(input, &this.shared),
			RefKindReflect::Unique(is) => is.ltrtl::<In, Out>(input, &this.unique),
		}
	}
}

impl<'a, 'b, In, Out, F, G> RefGenericFnK<'a, 'b, Unique, In, Out> for ChooseFn<F, G>
where
	In: RefGenericDataType<'a>,
	Out: RefGenericDataType<'b>,
	F: FnMut(In::Inst<Shared>) -> Out::Inst<Shared>,
	G: FnMut(In::Inst<Unique>) -> Out::Inst<Unique>,
{
	#[inline]
	fn call<K: RefKind>(this: &mut Self, input: In::Inst<K>) -> Out::Inst<K> {
		match K::reflect() {
			RefKindReflect::Shared(is) => is.ltrtl::<In, Out>(input, &mut this.shared),
			RefKindReflect::Unique(is) => is.ltrtl::<In, Out>(input, &mut this.unique),
		}
	}
}

/// A convenience function for turning a `K::Ref<'a, T>` into a `&'a T`
pub struct AsRef;

impl<'a: 'b, 'b, T: 'a + ?Sized, FK: RefKind>
	RefGenericFnK<'a, 'b, FK, SingleRef<T>, Fixed<&'b T>> for AsRef
{
	#[inline]
	fn call<K: RefKind>(_: FK::Ref<'_, Self>, input: K::Ref<'a, T>) -> &'b T {
		match input.reflect() {
			Reflect::Shared(r, _) => r,
			// auto-deref
			Reflect::Unique(r, _) => r,
		}
	}
}
impl<'a: 'b, 'b, T: 'a + ?Sized> RefGenericFnOnce<'a, 'b, SingleRef<T>, Fixed<&'b T>> for AsRef {
	fn call<K: RefKind>(self, input: K::Ref<'a, T>) -> &'b T {
		RefGenericFnK::<'a, 'b, Shared, _, _>::call::<K>(&self, input)
	}
}

/// A convenience function for turning a `K::Ref<'a, T>` into a `K::Ref<'a, T::Target>`
pub struct Deref;

impl<'a, T: 'a + ?Sized + DerefMut, FK: RefKind>
	RefGenericFnK<'a, 'a, FK, SingleRef<T>, SingleRef<T::Target>> for Deref
where
	T::Target: 'a,
{
	#[inline]
	fn call<K: RefKind>(
		_: FK::Ref<'_, Self>,
		input: K::Ref<'a, T>,
	) -> K::Ref<'a, T::Target> {
		RefGenericFnOnce::<'a, 'a, SingleRef<T>, SingleRef<T::Target>>::call::<K>(
			ChooseFn {
				shared: T::deref,
				unique: T::deref_mut,
			},
			input,
		)
	}
}
impl<'a, T: 'a + ?Sized + DerefMut> RefGenericFnOnce<'a, 'a, SingleRef<T>, SingleRef<T::Target>>
	for Deref
where
	T::Target: 'a,
{
	fn call<K: RefKind>(self, input: K::Ref<'a, T>) -> K::Ref<'a, T::Target> {
		RefGenericFnK::<'a, 'a, Shared, _, _>::call::<K>(&self, input)
	}
}

/// A function that converts a ref-generic struct to a tuple of some of its fields
#[macro_export]
macro_rules! field_project {
	($t:ty: $($field:ident),+ $(,)?) => {
		$crate::func::ChooseFn {
			shared: ::core::convert::identity::<fn(_) -> _>(|x: &$t| ($(&x.$field),*)),
			unique: ::core::convert::identity::<fn(_) -> _>(|x: &mut $t| ($(&mut x.$field),*)),
		}
	};
}

/// A function that applies a common method to both cases of a ref-generic value
#[macro_export]
macro_rules! forward {
	($s:ty, $u:ty : $meth:ident($($args:expr),* $(,)?)) => {
		$crate::func::ChooseFn {
			shared: |x: $s| x.$meth($($args)*),
			unique: |x: $u| x.$meth($($args)*),
		}
	};
}