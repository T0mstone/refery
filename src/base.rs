use core::convert::Infallible;

use crate::func::RefGenericFnOnce;
use crate::generic::{RefGenericDataType, SingleRef};
use crate::reflect::{Is, RefKindReflect, Reflect};

mod private {
	pub trait Sealed {}
}

/// The kind of a reference ([`Shared`] or [`Unique`])
pub trait RefKind: Sized + private::Sealed {
	/// The reference of this kind with lifetime `'a` and type `T`
	type Ref<'a, T: 'a + ?Sized>: Ref<'a, T, Kind = Self>;

	/// Select a type based on what reference kind this is
	type Select<S, U>;

	/// Obtain a runtime representation of this reference kind
	fn reflect() -> RefKindReflect<Self>;
}

/// A reference (`&T` or `&mut T`)
pub trait Ref<'a, T: 'a + ?Sized>: private::Sealed {
	/// The kind of this reference
	type Kind: RefKind<Ref<'a, T> = Self>;

	/// Map this reference to a different data type
	fn map<'b, D: RefGenericDataType<'b>>(
		self,
		map: impl RefGenericFnOnce<'a, 'b, SingleRef<T>, D>,
	) -> D::Inst<Self::Kind>;

	/// Obtain a runtime representation of this reference and its kind
	fn reflect(self) -> Reflect<'a, SingleRef<T>, Self::Kind>;
}

/// The reference kind of shared (`&T`) references
pub struct Shared(Infallible);
/// The reference kind of unique (`&mut T`) references
pub struct Unique(Infallible);

impl private::Sealed for Shared {}
impl private::Sealed for Unique {}
impl<'a, T: 'a + ?Sized> private::Sealed for &'a T {}
impl<'a, T: 'a + ?Sized> private::Sealed for &'a mut T {}

impl RefKind for Shared {
	type Ref<'a, T: 'a + ?Sized> = &'a T;

	type Select<S, U> = S;

	#[inline]
	fn reflect() -> RefKindReflect<Self> {
		RefKindReflect::Shared(Is::default())
	}
}
impl<'a, T: 'a + ?Sized> Ref<'a, T> for &'a T {
	type Kind = Shared;

	#[inline]
	fn map<'b, D: RefGenericDataType<'b>>(
		self,
		map: impl RefGenericFnOnce<'a, 'b, SingleRef<T>, D>,
	) -> D::Inst<Shared> {
		map.call::<Shared>(self)
	}

	#[inline]
	fn reflect(self) -> Reflect<'a, SingleRef<T>, Self::Kind> {
		Reflect::Shared(self, Is::default())
	}
}

impl RefKind for Unique {
	type Ref<'a, T: 'a + ?Sized> = &'a mut T;

	type Select<S, U> = U;

	#[inline]
	fn reflect() -> RefKindReflect<Self> {
		RefKindReflect::Unique(Is::default())
	}
}
impl<'a, T: 'a + ?Sized> Ref<'a, T> for &'a mut T {
	type Kind = Unique;

	#[inline]
	fn map<'b, D: RefGenericDataType<'b>>(
		self,
		map: impl RefGenericFnOnce<'a, 'b, SingleRef<T>, D>,
	) -> D::Inst<Unique> {
		map.call::<Unique>(self)
	}

	#[inline]
	fn reflect(self) -> Reflect<'a, SingleRef<T>, Self::Kind> {
		Reflect::Unique(self, Is::default())
	}
}
