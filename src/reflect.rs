//! Type-safe analysis of the ref-kind of a generic value using runtime code
//!
//! Most basically, this allows for matching on a generic [`RefKind`]:
//! ```
//! # use refery::{RefKind, reflect::RefKindReflect};
//! fn foo<K: RefKind>() -> u8 {
//!     match K::reflect() {
//!         RefKindReflect::Shared(_) => 1,
//!         RefKindReflect::Unique(_) => 2
//!     }
//! }
//! ```

use core::fmt::{Debug, Formatter};
use core::hash::{Hash, Hasher};

use crate::func::RefGenericFnOnce;
use crate::generic::RefGenericDataType;
use crate::*;

/// A marker type that denotes that `L` and `R` are the same type
///
/// # Safety
/// This type holds the condition `L == R` as a safety invariant.
/// It is UB to have a value of this type for `L != R`.
pub struct Is<L: RefKind, R: RefKind>(typewit::TypeEq<L, R>);
impl<K: RefKind> Default for Is<K, K> {
	fn default() -> Self {
		Self(Default::default())
	}
}

typewit::type_fn! {
	struct InstOf<'a, D: RefGenericDataType<'a>>;

	impl<K: RefKind> K => D::Inst<K>;
}

impl<L: RefKind, R: RefKind> Is<L, R> {
	/// Convert a value of the left kind to one of the right kind
	///
	/// This ultimately does nothing, but it is needed since Rust's type checker isn't powerful enough
	pub fn ltr<'a, D: RefGenericDataType<'a>>(&self, l: D::Inst<L>) -> D::Inst<R> {
		self.0.map(InstOf::<'a, D>::NEW).to_right(l)
	}

	/// Same as [`ltr`](Self::ltr) but in the other direction
	pub fn rtl<'a, D: RefGenericDataType<'a>>(&self, r: D::Inst<R>) -> D::Inst<L> {
		self.0.map(InstOf::<'a, D>::NEW).to_left(r)
	}

	/// Shortcut for a common pattern: `rtl(map(ltr(l)))`
	pub fn ltrtl<'a, 'b, D: RefGenericDataType<'a>, D2: RefGenericDataType<'b>>(
		&self,
		l: D::Inst<L>,
		map: impl FnOnce(D::Inst<R>) -> D2::Inst<R>,
	) -> D2::Inst<L> {
		self.rtl::<D2>(map(self.ltr::<D>(l)))
	}

	/// Shortcut for a common pattern: `ltr(map(rtl(l)))`
	pub fn rtltr<'a, 'b, D: RefGenericDataType<'a>, D2: RefGenericDataType<'b>>(
		&self,
		r: D::Inst<R>,
		map: impl FnOnce(D::Inst<L>) -> D2::Inst<L>,
	) -> D2::Inst<R> {
		self.ltr::<D2>(map(self.rtl::<D>(r)))
	}
}

impl<L: RefKind, R: RefKind> Copy for Is<L, R> {}
impl<L: RefKind, R: RefKind> Clone for Is<L, R> {
	fn clone(&self) -> Self {
		*self
	}
}
impl<L: RefKind, R: RefKind> PartialEq for Is<L, R> {
	fn eq(&self, _other: &Self) -> bool {
		true
	}
}
impl<L: RefKind, R: RefKind> Eq for Is<L, R> {}
impl<L: RefKind, R: RefKind> Hash for Is<L, R> {
	fn hash<H: Hasher>(&self, _state: &mut H) {}
}

/// A reflected value of `K`
#[allow(missing_docs)]
pub enum RefKindReflect<K: RefKind> {
	Shared(Is<K, Shared>),
	Unique(Is<K, Unique>),
}

impl<K: RefKind> Debug for RefKindReflect<K> {
	fn fmt(&self, f: &mut Formatter) -> core::fmt::Result {
		f.write_str(match self {
			RefKindReflect::Shared(_) => "Shared",
			RefKindReflect::Unique(_) => "Unique",
		})
	}
}
impl<K: RefKind> Copy for RefKindReflect<K> {}
impl<K: RefKind> Clone for RefKindReflect<K> {
	#[inline]
	fn clone(&self) -> RefKindReflect<K> {
		*self
	}
}
impl<K: RefKind> Eq for RefKindReflect<K> {}
impl<K: RefKind> PartialEq for RefKindReflect<K> {
	#[inline]
	fn eq(&self, other: &RefKindReflect<K>) -> bool {
		core::mem::discriminant(self) == core::mem::discriminant(other)
	}
}
impl<K: RefKind> Hash for RefKindReflect<K> {
	#[inline]
	fn hash<H: Hasher>(&self, state: &mut H) {
		core::mem::discriminant(self).hash(state);
	}
}

/// A reflected value of [`D::Inst`](RefGenericDataType::Inst)
///
/// The implementation of `PartialEq` only considers values of the same kind (`Shared, Shared` or `Unique, Unique`)
/// to be equal, even if the values would otherwise be equal.
#[allow(missing_docs)]
pub enum Reflect<'a, D: RefGenericDataType<'a>, K: RefKind> {
	Shared(D::Inst<Shared>, Is<K, Shared>),
	Unique(D::Inst<Unique>, Is<K, Unique>),
}

impl<'a, D: RefGenericDataType<'a>, K: RefKind> Reflect<'a, D, K> {
	/// Obtain the reference kind of this value
	pub fn kind(&self) -> RefKindReflect<K> {
		match *self {
			Reflect::Shared(_, is) => RefKindReflect::Shared(is),
			Reflect::Unique(_, is) => RefKindReflect::Unique(is),
		}
	}

	/// Apply a ref-generic function to this value
	pub fn map<'b, D2: RefGenericDataType<'b>>(
		self,
		map: impl RefGenericFnOnce<'a, 'b, D, D2>,
	) -> Reflect<'b, D2, K> {
		use Reflect::*;
		match self {
			Shared(inst, is) => Shared(map.call::<crate::Shared>(inst), is),
			Unique(inst, is) => Unique(map.call::<crate::Unique>(inst), is),
		}
	}
}

impl<'a, D: RefGenericDataType<'a>, K: RefKind> Debug for Reflect<'a, D, K>
where
	D::Inst<Shared>: Debug,
	D::Inst<Unique>: Debug,
{
	fn fmt(&self, f: &mut Formatter) -> core::fmt::Result {
		match self {
			Reflect::Shared(inst, _) => f.debug_tuple("Shared").field(inst).finish(),
			Reflect::Unique(inst, _) => f.debug_tuple("Unique").field(inst).finish(),
		}
	}
}
impl<'a, D: RefGenericDataType<'a>, D2: RefGenericDataType<'a>, K: RefKind, K2: RefKind>
	PartialEq<Reflect<'a, D2, K2>> for Reflect<'a, D, K>
where
	D::Inst<Shared>: PartialEq<D2::Inst<Shared>>,
	D::Inst<Unique>: PartialEq<D2::Inst<Unique>>,
{
	#[inline]
	fn eq(&self, other: &Reflect<'a, D2, K2>) -> bool {
		use Reflect::*;
		match (self, other) {
			(Shared(linst, _), Shared(rinst, _)) => linst == rinst,
			(Unique(linst, _), Unique(rinst, _)) => linst == rinst,
			_ => false,
		}
	}
}
impl<'a, D: RefGenericDataType<'a>, K: RefKind> Eq for Reflect<'a, D, K>
where
	D::Inst<Shared>: Eq,
	D::Inst<Unique>: Eq,
{
}
impl<'a, D: RefGenericDataType<'a>, K: RefKind> Hash for Reflect<'a, D, K>
where
	D::Inst<Shared>: Hash,
	D::Inst<Unique>: Hash,
{
	#[inline]
	fn hash<H: Hasher>(&self, state: &mut H) {
		core::mem::discriminant(self).hash(state);
		match self {
			Self::Shared(inst, _) => inst.hash(state),
			Self::Unique(inst, _) => inst.hash(state),
		}
	}
}
