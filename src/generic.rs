//! Data types generic over a [`RefKind`]

use crate::*;

/// A ref-generic data type, i.e. a data type that is generic over a [`RefKind`]
pub trait RefGenericDataType<'a> {
	/// The concrete type for the given `RefKind`
	type Inst<K: RefKind>;
}

/// This impl is a convenience shortcut, making `()` work the same as [`Fixed`]`<()>`
impl<'a> RefGenericDataType<'a> for () {
	type Inst<K: RefKind> = ();
}

impl<'a, D> RefGenericDataType<'a> for Option<D>
where
	D: RefGenericDataType<'a>,
{
	type Inst<K: RefKind> = Option<D::Inst<K>>;
}

impl<'a, D, E> RefGenericDataType<'a> for Result<D, E>
where
	D: RefGenericDataType<'a>,
	E: RefGenericDataType<'a>
{
	type Inst<K: RefKind> = Result<D::Inst<K>, E::Inst<K>>;
}

/// A fixed type that is always `T`, independent of the kind of reference used
pub struct Fixed<T>(PhantomNever<T>);

impl<'a, T> RefGenericDataType<'a> for Fixed<T> {
	type Inst<K: RefKind> = T;
}

/// A single reference (of the given kind) to a `T`
pub struct SingleRef<T: ?Sized>(PhantomNever<T>);

impl<'a, T: 'a + ?Sized> RefGenericDataType<'a> for SingleRef<T> {
	type Inst<K: RefKind> = K::Ref<'a, T>;
}

#[deprecated = "Use `(SingleRef<T>, Ts)` instead"]
/// A generic combinator for adding a reference type to the start of a ref-generic data type,
/// thus allowing for arbitrary-sized (nested) tuples as ref-generic data types.
pub struct RefAnd<T: ?Sized, Ts>(PhantomNever<(Ts, T)>);

#[allow(deprecated)]
impl<'a, T: 'a + ?Sized, Ts> RefGenericDataType<'a> for RefAnd<T, Ts>
where
	Ts: RefGenericDataType<'a>,
{
	type Inst<K: RefKind> = (K::Ref<'a, T>, Ts::Inst<K>);
}


macro_rules! impl_tuple {
    (@gen $($t:ident)+) => {
		/// A tuple of ref-generic values
		impl<'a, $($t: RefGenericDataType<'a>),+> RefGenericDataType<'a> for ($($t,)+) {
			type Inst<K: RefKind> = ($(<$t as RefGenericDataType<'a>>::Inst<K>,)+);
		}
	};
	(@nom [$($ts:ident)*] $t:ident $($r:tt)*) => {
		impl_tuple!(@gen $($ts)* $t);
		impl_tuple!(@nom [$($ts)* $t] $($r)*);
	};
	(@nom [$($ts:ident)*] ) => {};
	($($t:ident)+) => {
		impl_tuple!(@nom [] $($t)+);
	};
	([$($tl:ident)*] $($t:ident)+) => {
		impl_tuple!(@nom [$($tl)*] $($t)+);
	};
}

impl_tuple!(D0 D1 D2 D3);
#[cfg(feature = "tuples-to-32")]
impl_tuple!(
	[D0  D1  D2  D3] D4  D5  D6  D7
	D8  D9  DA  DB   DC  DD  DE  DF
	D10 D11 D12 D13  D14 D15 D16 D17
	D18 D19 D1A D1B  D1C D1D D1E D1F
	D20 D21 D22 D23  D24 D25 D26 D27
	D28 D29 D2A D2B  D2C D2D D2E D2F
	D30 D31 D32 D33  D34 D35 D36 D37
	D38 D39 D3A D3B  D3C D3D D3E D3F
);