//! # Refery
//!
//! A GAT-fest of reference abstraction fuckery
//!
//! ## Example
//! With this crate, you can abstract
//! ```
//! fn takes_ref(x: &u8) {
 //!   // ...
//! }
//!
//! fn takes_mut(x: &mut u8) {
//!    // ...
//! }
//! ```
//! into a single function
//! ```
//! use refery::RefKind;
//! fn takes_both<K: RefKind>(x: K::Ref<'_, u8>) {
//!    // ...
//! }
//! ```
//! Sadly, this doesn't work with methods because `self: K::Ref<'_, _>` isn't allowed at the moment.
//!
//! ## Crate Features
//! - `tuples-to-32` (enabled by default): Generate [`RefGenericDataType`](generic::RefGenericDataType) impls for tuples up to length 32 (instead of 4)
#![no_std]
#![warn(missing_docs)]

use core::convert::Infallible;
use core::marker::PhantomData;

pub use crate::base::*;

// invariant wrapper that has no possible values
struct PhantomNever<T: ?Sized> {
	// note: `*mut T` is invariant in `T` (see https://doc.rust-lang.org/reference/subtyping.html#variance)
	_marker: PhantomData<*mut T>,
	_never: Infallible,
}

mod base;
pub mod generic;
pub mod func;
pub mod reflect;